#!/bin/bash
# convert png to eps with provided dpi

dpi=$1

echo "Converting png --> eps with $dpi dpi."

cd png
for files in $(ls *.png)
do
    filename=$(basename "$files")
    extension="${filename##*.}"
    filename="${filename%.*}"
    convert -quality 100 -density $dpi $filename.png ../eps/$filename.eps
done
cd ..
