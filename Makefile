default:
	xelatex main
	#bibtex  main
	xelatex main
	xelatex main

tex:
	latexmk -pdf -pvc main

overview-tex:
	latexmk -pdf -pvc overview

eps:
	cd figures && bash png2eps.sh 300; cd ..

clean:
	rm -rf *aux *.bbl *.blg *.fdb_latexmk  *.fls *.lof *.log *.lot *.out *.toc figures/eps/*-eps-converted-to.pdf

clean-all:
	make clean
	rm main.pdf
