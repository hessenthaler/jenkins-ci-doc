%===============================================================================
%
\section{Jenkins server}\label{jenkins-sec}
%
The CBM group has started following good software development and CI principles
more closely. As part of the new strategy, we use a Jenkins server for automated
testing on a production machine.

Here, we provide some detail about what we test (at the moment), why we test
and how we do it.
%
%===============================================================================
%
\subsection{New development workflow}\label{new-development-workflow-sec}
%
The new workflow is now:
%
\begin{enumerate}
    \setlength\itemsep{0.01em}
    \item{make changes in working copy
        \label{workflow2-changes-enum}}
    \item{if code does not build on local machine,
        go back to \ref{workflow2-changes-enum}
        \label{workflow2-build-enum}}
    \item{if there is a single failed test on local machine,
        go back to \ref{workflow2-changes-enum}
        \label{workflow2-tests-enum}}
    \item{if there are changes in the remote code base,
        merge those and go back to \ref{workflow2-build-enum}
        \label{workflow2-merge-upstream-enum}}
    \item{now, commit and push changes to own github fork}
    \item{trigger the CI pipeline for your github fork on the production
        machine\label{workflow2-trigger-CI-pipeline-enum}}
    \subitem{- either via the web GUI of your Jenkins server}
    \subitem{- or by browsing to a predefined weblink}
    \item{if there is an error in the automated CI pipeline,
        go back to \ref{workflow2-changes-enum}
        \label{workflow2-failed-test-lead-enum}}
    \item{pull-request to get your changes into the common code base
        \label{workflow2-pull-request-enum}}
    \item{another developer reviews and accepts pull request on \github
        \label{workflow2-accept-pull-request-enum}}
    \item{begin with your next implementation task,
        i.e.\ go back to \ref{workflow2-changes-enum}}
\end{enumerate}
%
With this new workflow the number of total steps has decreased. But more
importantly, the number of \textbf{manual} steps that have to be done remotely
has decreased as testing on the production machine is completely automated.
%
%===============================================================================
%
\subsubsection{Further work}\label{further-work-sec}
%
Ultimately, Step~\ref{workflow2-trigger-CI-pipeline-enum}
in Section~\ref{new-development-workflow-sec} will be triggered automatically
once a code change is discovered for a given \github\ repository. Once the
automated build/tests were successful, the code changes will be pulled into the
common code base automatically, such that Step~\ref{workflow2-pull-request-enum}
and Step~\ref{workflow2-accept-pull-request-enum}
become fully automated as well.
%
%===============================================================================
%
\subsection{Get Jenkins up and running on a Linux machine}\label{jenkins-setup-sec}
%
Create a user \emph{jenkins} on your production (Linux) machine to administrate
your Jenkins server. Download \emph{Java 8} (or newer)
and \emph{Jenkins 2.32} (or newer).

Then, start the Jenkins server using \emph{nohup}\footnote{It is best if you
write a script that automates the described tasks}:
%
\begin{verbatim}
    $ DATE=`date +%Y-%m-%d:%H:%M:%S`
    $ nohup java -Xmn128M -Xms4096M -Xmx8192M -jar jenkins.war \
    $   --httpPort=8081 --prefix=/jenkins \
    $   --argumentsRealm.passwd.jenkins=init \
    $   --argumentsRealm.roles.jenkins=admin \
    $   > $DATE.jenkins.log 2>&1&
    $ echo $! > save_pid.txt
\end{verbatim}
%
which limits memory usage (see Java
docs\footnote{\url{https://docs.oracle.com/javase/8/docs/technotes/tools/windows/java.html}}),
sets the port for the web login, stores the PID of the process, etc.

Using above command, there will be logs for each instance of the Jenkins server,
i.e., there will be a new log once the Jenkins server process is killed and
restarted. The respective logs can be identified by the time and date a Jenkins
server instance is started.

We kill the Jenkins server based on its PID on start-up
%
\begin{verbatim}
    $ kill -9 `cat save_pid.txt`
\end{verbatim}
%
and administrate it through the weblogin at
%
\begin{verbatim}
    $ firefox hostname:8081/jenkins &
\end{verbatim}
%
where \emph{hostname} is the name of your production
machine\footnote{On Linux, run: echo \$HOSTNAME}
or its IP address\footnote{On Linux, run: ifconfig}.
Then use your initial login credentials that you had set above
(user: admin; password: init) and change it immediately. Then you can also add
new users by going to \emph{Manage Jenkins}.
%
%===============================================================================
%
\subsection{Jenkins pipeline}\label{jenkins-pipeline-sec}
%
Simply add a \emph{New Item} at the landing page of the Jenkins server, enter
a name and configure your own pipeline (check the docs) or copy an existing
pipeline (that you can then modify). An example is following.
%
\subsubsection{Example pipeline}\label{example-pipeline-sec}
%
Here's a very simple example pipeline, that has two \emph{stages} (these could
be your build and testing steps, for example).
On the Jenkins server webpage, click on \emph{New Item}, type in a name and
select \emph{Pipeline}. On the next page under \emph{General} go with the
defaults. Under \emph{Build Triggers}, toggle \emph{Trigger builds remotely}
and choose an \emph{Authentication Token} (write down the URL and the token).
Finally, under \emph{Pipeline} select \emph{Pipeline script} and replace its
contents by the following:
%
\begin{verbatim}
    node ('lead') {
        stage('Preparation') {
            // First stage
            sh 'rm -f testfile.log'
            sh 'touch testfile.log'
        }
        stage('Test') {
            // Second stage
            // the last line in a stage always determines
            // failure/success by the exit code of the last command
            sh 'cat testfile.log | wc -l | grep -q 0'
        }
    }
\end{verbatim}
%
Hit \emph{Save}. On the Jenkins server's startpage, you will now see the
pipeline. Click on it and then on \emph{Build}. This will execute your pipeline
and you will see that all tasks were completed successfully.

If you would like to simulate a broken pipeline, configure the pipeline and
replace the script by the following:
%
\begin{verbatim}
    node ('lead') {
        stage('Preparation') {
            // First stage
            sh 'rm -f testfile.log'
            sh 'touch testfile.log'
            // add a line to emulate a failed test
            sh 'echo "failed test" >> testfile.log'
        }
        stage('Test') {
            // Second stage
            // the last line in a stage always determines
            // failure/success by the exit code of the last command
            sh 'cat testfile.log | wc -l | grep -q 0'
        }
    }
\end{verbatim}
%
Above scripts result in one successful and one failed test,
see Figure~\ref{example-pipeline-tests-fig}. By hovering your mouse pointer
over the failed stage, you can see what went wrong by clicking \emph{Logs} in
the pop-up window.
%
\begin{figure}[h!]
    \centering 
    \includegraphics[width=\columnwidth]{figures/png/pipeline_examples_passed-failed.png} 
    \caption{Example of failed/passed pipeline stages.}
    \label{example-pipeline-tests-fig}
\end{figure}
%
%===============================================================================
%
\subsection{Recommended plugins}\label{recommended-plugins-sec}
%
Plugins can be installed via \emph{Manage Jenkins} $\rightarrow$
\emph{Plugin Manage}. Recommended Jenkins plugins (in addition to the
pre-installed ones) are:
%
\begin{itemize}
    \setlength\itemsep{0.01em}
    \item{Pipeline: enables building build/test pipelines as described in
        Section~\ref{jenkins-pipeline-sec}}
    \item{GitHub Organization Folder Plugin: allows watching \github\
        repositories and trigger builds when code changes are discovered}
    \item{Node and Label parameter plugin: enables triggering builds on remote
        machines, e.g., a specific compute node on your cluster}
    \item{SSH Agent Plugin/SSH Slaves plugin: enables triggering builds on remote
        machines that only offer ssh access}
    \item{Workspace Cleanup Plugin: allows to delete the temporary workspace
        after executing a build pipeline}
\end{itemize}
%
%===============================================================================
