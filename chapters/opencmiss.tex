%===============================================================================
%
\section{Development of OpenCMISS}\label{development-opencmiss-sec}
%
At the Continuum Biomechanics and Mechanobiology Research Group (CBM group)
at the University of Stuttgart, we are contributing to the
\opencmiss\footnote{\url{opencmiss.org}}\ code base.
\opencmiss\ is an open source software tool for scientific modeling, simulation
and visualization.

The official \opencmiss\ code is hosted
on \github\footnote{\url{github.org/OpenCMISS}}\
and thus under version control using \git\footnote{Note: We assume that you are
familiar with \git. Guide: \url{https://www.atlassian.com/git}}.
\opencmiss\ consists of install scripts, the \dependencies, \iron, \zinc\ and
some \examples.

Our development efforts are mainly focused
on \iron\footnote{\url{github.org/OpenCMISS/iron}},
which is the computational back-end of the software package,
and the \examples, which are the front-end for running simulations using \iron.
Thus, the CBM group has their
own \iron\ fork\footnote{\url{github.org/cbm-software/iron}} and again,
all developers have their own fork.

While the CBM group's fork is intended to
provide a \textbf{stable} development version, each developer may also commit
intermediate changes to their \textbf{own} \github\ fork at their own
responsibility.

The \dependencies\ can be installed in a central user-accessible location and
linked when installing \iron.
%
%===============================================================================
%
\subsection{State of the Art before using Jenkins}\label{setup-before-jenkins-sec}
%
The main installation steps for \opencmiss\ are manual:
%
\begin{itemize}
    \setlength\itemsep{0.01em}
    \item{git clone to get local copy of source files}
    \item{cmake to configure build step}
    \item{build step to compile the configured libraries}
    \item{testing of installed libraries}
\end{itemize}
%
The install scripts are cloned from a central
repository\footnote{\url{github.org/OpenCMISS/manage}}.
Then, configuration of builds is done using \cmake,
which essentially checks the build environment for the prerequisites and
installs operating system-specific Makefiles.
A typical configuration run is performed via the command:
%
\begin{verbatim}
    $ cmake -DEVIL=1 \
        -DCMAKE_BUILD_TYPE=DEBUG \
        -DOPENCMISS_BUILD_TYPE=DEBUG
        -DOPENCMISS_SDK_DIR=~/opencmiss/dependencies/install ..
\end{verbatim}
%
The generated main Makefile provides the following targets:
%
\begin{itemize}
    \setlength\itemsep{0.01em}
    \item{\emph{make opencmiss} - builds the \dependencies\ and/or \iron, i.e.,
        in the same or in separate directories}
    \item{\emph{make keytests} - builds and runs the keytests, a set of
        \examples\ that are used to compare simulation results with expected
        results for s subset of the implemented equations}
\end{itemize}
%
{\color{gray}
Note: There are plenty other targets, but these are the important ones for this
document.}
%
%===============================================================================
%
\subsubsection{Typical development workflow}\label{typical-workflow-before-jenkins-sec}
%
We are usually installing the \dependencies\ in a central location and link
against the \dependencies\ when we build \iron, i.e., they are installed in two
separate directories. Here, we only explain the workflow for developing \iron.

That is, a typical development workflow before introducing a Jenkins server
used to look like this:
%
\begin{enumerate}
    \setlength\itemsep{0.01em}
    \item{make changes in working copy
        \label{workflow-changes-enum}}
    \item{if code does not build on local machine,
        go back to \ref{workflow-changes-enum}
        \label{workflow-build-enum}}
    \item{if there is a single failed test on the local machine,
        go back to \ref{workflow-changes-enum}
        \label{workflow-tests-enum}}
    \item{if there are changes in the remote code base,
        merge those and go back to \ref{workflow-changes-enum}
        \label{workflow-merge-upstream-enum}}
    \item{now, commit and push changes to own \github\ fork}
    
    \item{login to our small compute cluster \lead\ and clone your \github\ fork}
    \item{if code does not build on cluster,
        go back to \ref{workflow-changes-enum}
        \label{workflow-build-cluster-enum}}
    \item{if there is a single failed test on the cluster,
        go back to \ref{workflow-changes-enum}
        \label{workflow-tests-enum}}
    \item{if there are changes in the remote code base,
        merge those and go back to \ref{workflow-changes-enum}
        \label{workflow-merge-upstream-enum}}
    \item{pull-request to get your changes into the common code base,
        i.e., CBM's \github\ fork}
    \item{another developer reviews and accepts pull-request on \github\
        (or: declines pull-request, asks for revisions)}
    \item{begin with your next implementation task,
        i.e.\ go back to \ref{workflow-changes-enum}}
\end{enumerate}
%
%===============================================================================
%
\subsubsection{Potential risks}\label{potential-risks-sec}
%
The workflow described in Section~\ref{typical-workflow-before-jenkins-sec}
is a lengthy (and sometimes annoying) process with many manual steps.
As such, there is the risk that developers will shortcut and not thoroughly
test their changes (or only on their local machine) before their code is pulled
into the main code base.

Another risk is that developers will make many changes and implement many
features to reduce the number of commits and manual build/testing steps that
they have to perform.

Thus, the common code base is prone to break due to changes which have not been
tested as much as they should have been.
Even worse, bug tracking becomes tedious due to the size of the commits and as
such, the time to resolve bugs and fix the common code base for everyone
increases.

And as you might have noticed, this workflow is not in line with CI principles
and good practice of software development.
%
%===============================================================================
